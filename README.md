# OnlineShopRestApi 



## API

This is a Rest api for a online shop in which the following things are implemented:

- /api/item:

    - /all - get all items with paginate
 
    - /show/{item} - get data for item page
 
    - /delete/{item} - delete item from DB (soft delete)
 
    - /store - save item in DB
     
    - /update/{item} - update data item in DB

- /api/order:

    - /all - get all orders with paginate
    - /show/{order} - get data for order page
    - /delete/{order} - delete order from DB (soft delete)
    - /store - save order in DB
    - /update/{order} -  update data order in DB
    - /{order}/items - get all the items of an order
    - /status/{order}/success - set status order success
    - /status/{order}/decline - set status order decline
    - /status/{order}/pending - set status order pending

In order to run the project:

```
docker-compose build
docker-compose up -d
docker-compose exec web python manage.py makemigrations
docker-compose exec web python manage.py migrate
```


Link for the API documentation: http://127.0.0.1:8000/swagger/

Every endpoint can be found in the documentation and can be used from there. 

I added a Postgresql database to the docker file, and added a volume so that data is persistent, even after shutting down the container. The modules I used are the following: 
- django 

- djangorestframework 

- psycopg2-binary (needed for postgresql)

- drf-yasg (for the API documentation)

<br>

### TODOs

1. Implement tests!
