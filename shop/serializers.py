from rest_framework import serializers
from .models import Item, Order, OrderItem
from django.utils import timezone

class ItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = '__all__'

class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    order_items = OrderItemSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'customer_name', 'status', 'created_at', 'updated_at', 'order_items')

class OrderItemCreateSerializer(serializers.ModelSerializer):
    item = serializers.PrimaryKeyRelatedField(queryset=Item.objects.filter(is_deleted=False), required=False)


    class Meta:
        model = OrderItem
        fields = ('item', 'quantity')

class OrderCreateSerializer(serializers.ModelSerializer):
    order_items = OrderItemCreateSerializer(many=True)
    class Meta:
        model = Order
        fields = ('customer_name', 'order_items')

    def create(self, validated_data):
        print(f"Data: {validated_data}")
        order_items_data = validated_data.pop('order_items')
        order = Order.objects.create(**validated_data)
        for item_data in order_items_data:
            item_instance = item_data.pop('item')
            OrderItem.objects.create(order=order, item=item_instance, **item_data)
        return order

class OrderItemUpdateSerializer(serializers.ModelSerializer):
    item = serializers.PrimaryKeyRelatedField(queryset=Item.objects.filter(is_deleted=False), required=False)

    class Meta:
        model = OrderItem
        fields = ('item', 'quantity')


class OrderUpdateSerializer(serializers.ModelSerializer):

    order_items = OrderItemUpdateSerializer(many=True)

    class Meta:
        model = Order
        fields = ('order_items',)

    def update(self, instance, validated_data):
        
        instance.updated_at = timezone.now()
        instance.save()

        order_items_data = validated_data.pop('order_items')

        instance.order_items.all().delete()

        for item_data in order_items_data:
            item_instance = item_data.pop('item')
            OrderItem.objects.create(order=instance, item=item_instance, **item_data)

        return instance    


class OrderItemsWithQuantitySerializer(serializers.Serializer):
    item = ItemSerializer()
    quantity = serializers.IntegerField()
