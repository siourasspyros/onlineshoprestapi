from django.urls import path 
from .views import (ItemList, ItemDetail, ItemStore, ItemSoftDelete, 
                    ItemUpdate, OrderList, OrderStore,OrderDetail, OrderStatusSuccess, 
                    OrderSoftDelete, OrderStatusDecline, 
                    OrderStatusPending,OrderItemsList,
                    OrderUpdate,)

urlpatterns = [
    path('item/all', ItemList.as_view(), name='item-list'),
    path('item/show/<int:pk>/', ItemDetail.as_view(), name='item-detail'),
    path('item/store/', ItemStore.as_view(), name='item-store'),
    path('item/delete/<int:pk>/', ItemSoftDelete.as_view(), name='item-delete'),
    path('item/update/<int:pk>/', ItemUpdate.as_view(), name='item-update'),
    path('order/all', OrderList.as_view(), name='order-list'),
    path('order/store/', OrderStore.as_view(), name='order-create'),
    path('order/show/<int:pk>/', OrderDetail.as_view(), name='order-detail'),
    path('order/delete/<int:pk>/', OrderSoftDelete.as_view(), name='order-delete'),
    path('order/update/<int:pk>/', OrderUpdate.as_view(), name='order-update'),

    path('order/status/<int:pk>/success/', OrderStatusSuccess.as_view(), name='order-status-success'),
    path('order/status/<int:pk>/decline/', OrderStatusDecline.as_view(), name='order-status-decline'),
    path('order/status/<int:pk>/pending/', OrderStatusPending.as_view(), name='order-status-pending'),

    path('order/<int:order_id>/items/', OrderItemsList.as_view(), name='order-items'),
]