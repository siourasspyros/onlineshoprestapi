from rest_framework import generics, filters, status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Item, Order, OrderItem
from .serializers import (ItemSerializer, OrderSerializer, 
                          OrderItemSerializer, 
                          OrderCreateSerializer, OrderUpdateSerializer,
                          OrderItemsWithQuantitySerializer,
                          )

# Create your views here.


class ItemList(generics.ListAPIView):
    queryset = Item.objects.filter(is_deleted = False)
    serializer_class = ItemSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description']

class ItemStore(generics.CreateAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    allowed_methods = ["POST"]

class ItemSoftDelete(APIView):

    allowed_methods = ['POST']
    def post(self, request, pk, format=None):
        item = Item.objects.get(pk=pk)
        if item.is_deleted == True:
            return Response(data="Item already deleted!", status=status.HTTP_400_BAD_REQUEST)
        item.is_deleted = True
        item.save()
        return Response('Item was soft deleted', status=status.HTTP_200_OK)

class ItemDetail(generics.RetrieveUpdateAPIView):
    queryset = Item.objects.filter(is_deleted = False)
    serializer_class = ItemSerializer
    allowed_methods = ['GET']


class ItemUpdate(generics.UpdateAPIView):
    queryset = Item.objects.filter(is_deleted=False)
    serializer_class = ItemSerializer

class OrderList(generics.ListAPIView):
    queryset = Order.objects.filter(is_deleted = False)
    serializer_class = OrderSerializer

class OrderStore(generics.CreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderCreateSerializer
    allowed_methods = ["POST"]

class OrderDetail(generics.RetrieveUpdateAPIView):
    queryset = Order.objects.filter(is_deleted = False)
    serializer_class = OrderSerializer
    allowed_methods = ['GET']

class OrderItems(generics.ListAPIView):
    serializer_class = OrderItemSerializer

    def get_queryset(self):
        order_id = self.kwargs['order']
        return OrderItem.objects.filter(order_id=order_id)
    
class OrderStatusSuccess(APIView):

    allowed_methods = ['POST']
    def post(self, request, pk, format=None):
        order = Order.objects.get(pk=pk)
        if order.status == Order.SUCCESS:
            return Response(data="Order already changed to success!", status=status.HTTP_400_BAD_REQUEST)
        order.status = order.SUCCESS
        order.save()
        return Response('Order was set to success', status=status.HTTP_200_OK)

class OrderStatusDecline(APIView):

    allowed_methods = ['POST']
    def post(self, request, pk, format=None):
        order = Order.objects.get(pk=pk)
        if order.status == Order.DECLINED:
            return Response(data="Order already changed to decline!", status=status.HTTP_400_BAD_REQUEST)
        order.status = order.DECLINED
        order.save()
        return Response('Order was set to decline', status=status.HTTP_200_OK)

class OrderStatusPending(APIView):

    allowed_methods = ['POST']
    def post(self, request, pk, format=None):
        order = Order.objects.get(pk=pk)
        if order.status == Order.PENDING:
            return Response(data="Order already changed to success!", status=status.HTTP_400_BAD_REQUEST)
        order.status = order.PENDING
        order.save()
        return Response('Order was set to pending', status=status.HTTP_200_OK)

class OrderSoftDelete(APIView):

    allowed_methods = ['POST']
    def post(self, request, pk, format=None):
        order = Order.objects.get(pk=pk)
        if order.is_deleted == True:
            return Response(data="Item already deleted!", status=status.HTTP_400_BAD_REQUEST)
        order.is_deleted = True
        order.save()
        return Response('Item was soft deleted', status=status.HTTP_200_OK)

class OrderUpdate(generics.UpdateAPIView):
    queryset = Order.objects.filter(is_deleted=False)
    serializer_class = OrderUpdateSerializer



class OrderItemsList(generics.ListAPIView):
    serializer_class = OrderItemsWithQuantitySerializer

    def get_queryset(self):
        order_id = self.kwargs.get('order_id')
        order_items = OrderItem.objects.filter(order__id=order_id)
        return order_items

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        # Serialize order_items with quantity
        serializer = self.get_serializer(queryset, many=True)
        serialized_order_items = serializer.data

        # Serialize items with quantity
        serialized_items_with_quantity = []
        for order_item in serialized_order_items:
            item_data = order_item['item']
            item_data['quantity'] = order_item['quantity']
            serialized_items_with_quantity.append(item_data)

        return Response(serialized_items_with_quantity)

